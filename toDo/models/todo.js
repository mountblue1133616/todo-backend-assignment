const { Sequelize } = require("sequelize");
require("dotenv").config(); // load environment variables from .env file

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "postgres",
  }
);

const Todo = sequelize.define("todo", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  text: Sequelize.STRING,
  isCompleted: Sequelize.BOOLEAN,
});

module.exports = Todo;
