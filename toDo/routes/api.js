var express = require("express");
var router = express.Router();
const Todo = require(`../models/todo`);
const yup = require("yup");

const todoSchema = yup.object().shape({
  text: yup.string().required(),
  isCompleted: yup.boolean().required(),
});

//Validation middleware
const validateTodo = async (req, res, next) => {
  try {
    await todoSchema.validate(req.body);
    next();
  } catch (error) {
    next(error); // Pass the error to the next middleware
  }
};

//Get all todos
router.get("/todos", async (req, res, next) => {
  try {
    const allTodos = await Todo.findAll();
    res.status(200).json(allTodos);
  } catch (error) {
    next(error); // Pass the error to the next middleware
  }
});

//Get particular todo
router.get("/todos/:id", async (req, res, next) => {
  try {
    const singleTodo = await Todo.findByPk(req.params.id);
    if (singleTodo == null) {
      res.status(404).json({ message: "Todo not found" });
    } else {
      res.status(200).json(singleTodo);
    }
  } catch (error) {
    next(error); // Pass the error to the next middleware
  }
});

//Create todos
router.post("/todos", validateTodo, async (req, res, next) => {
  try {
    const newTodo = await Todo.create(req.body);
    res.status(201).json(newTodo);
  } catch (error) {
    next(error); // Pass the error to the next middleware
  }
});

//Update a particular todo
router.put("/todos/:id", validateTodo, async (req, res, next) => {
  try {
    const todo = await Todo.findByPk(req.params.id);
    if (todo === null) {
      res.status(404).json({ message: "Todo not found" });
    } else {
      const updatedTodo = await todo.update(req.body);
      res.status(200).json(updatedTodo);
    }
  } catch (error) {
    next(error); // Pass the error to the next middleware
  }
});

//Delete todo
router.delete("/todos/:id", async (req, res, next) => {
  try {
    const todo = await Todo.findByPk(req.params.id);
    if (todo === null) {
      res.status(404).json({ message: "Todo not found" });
    } else {
      todo.destroy();
      res.status(200).json({ message: "Deleted todo sucessfully" });
    }
  } catch (error) {
    next(error); // Pass the error to the next middleware
  }
});

module.exports = router;
