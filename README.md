**Todo API**

  

This is a simple RESTful API for managing todo items. It provides endpoints for creating, updating, deleting, and retrieving todo items.

  

**Getting Started**

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

  

**Prerequisites**

- Node.js

- PostgreSQL

  

**Installing**

  

1. Clone the repository:

  

    git clone https://gitlab.com/mountblue1133616/todo-backend-assignment.git

2. Install the dependencies:

  

    cd toDo

    npm install

  

3. Configure the environment variables:

  

    cp .env.example .env
Update the .env file with YOUR PostgreSQL database credentials.

  

4. Set up the database

  

5. Start the server:

  

    npm start
    By default, the server runs on http://localhost:3000.

  

**API Documentation**

  

Todos

  

   Get all todos

  

	

**GET /api/todos**

  

*Description:Retrieves all todo items.`*

  

- Get a specific todo

  

**GET /api/todos/:id**

  

*Description:Retrieves a specific todo item based on the provided id parameter.*

  

- Create a todo

  

**POST /api/todos**

  

*Description:Creates a new todo item. The request body should contain the following JSON data:*

  

{

"text": "Task description",

"isCompleted": false

}

  

- Update a todo

  

**PUT /api/todos/:id**

  

*Description:Updates an existing todo item with the provided id. The request body should contain the updated JSON data:*

  

{

"text": "Updated task description",

"isCompleted": true

}

  

- Delete a todo

  

**DELETE /api/todos/:id**

  

*Description:Deletes the todo item with the provided id.*

  

**Error Handling**

  

If an error occurs during the API requests, the server will respond with an appropriate error message and status code. Common error codes include:

  

- 400 Bad Request: Invalid request or missing required data.

- 404 Not Found: The requested resource does not exist.

- 500 Internal Server Error: An unexpected server error occurred.

  

**Built With**

  

- Express.js - Web framework for Node.js

- Sequelize - ORM for interacting with the database

- Yup - Validation library

- PostgreSQL - Relational database management system

